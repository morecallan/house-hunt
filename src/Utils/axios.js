import axios from 'axios'


export const req = axios.create({
  baseURL: 'https://realtor.p.rapidapi.com/properties/v2/',
  timeout: 10000,
  headers: {
    'x-rapidapi-host': 'realtor.p.rapidapi.com',
    'x-rapidapi-key': 'b812e0cc23msh9376881e84a07afp13c2f0jsnd8d7488da8b2'
  }
})