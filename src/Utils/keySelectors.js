import { createSelector } from 'reselect'

export const keySelectors = (store, keys) => keys.reduce(
  (acc, k) => {
    acc[k] = createSelector(store, s => s[k])
    return acc
  },
  {}
)
