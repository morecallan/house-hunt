import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'

import { propertiesActions } from '../../state/ducks/properties'
import * as propertiesSelectors from '../../state/ducks/properties/selectors'
import { ResultsView } from './ResultsView'

const ResultsRender = (props) => (
  <ResultsView {...props} />
)

const mapStateToProps = (state) => ({
  properties: propertiesSelectors.properties(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getProperties: propertiesActions.getProperties,
}, dispatch)


const lifecycleHooks = lifecycle({
  async componentDidMount () {
    try {
      this.props.getProperties()
    } catch {
      console.log('ew no')
    }
  }
})


export const  ResultsContainer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycleHooks
)(ResultsRender)