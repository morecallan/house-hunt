import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { Link } from '@material-ui/core'


const styles = () => ({
  subheader: {
    color: 'blue'
  }
})

const ResultsRender = ({ classes, properties, ...rest }) => {
  return (
    <>
      {
        properties.map(p => (
          <>
          <Button>Cool</Button>
            <p>{p.price}</p>
            <p>{p.address.line}</p>
            <img src={p.thumbnail} alt='House Thumbnail' />
            <Link href={p.rdc_web_url}> Check it</Link>
          </>
        ))   
      }
    </>
  )
}
ResultsRender.displayName = 'ResultsRender'

const ResultsView = withStyles(styles)(ResultsRender)
ResultsView.displayName = 'ResultsView'

export { ResultsView }