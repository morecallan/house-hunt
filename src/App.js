import React from 'react'
import { withStyles } from '@material-ui/core/styles'

// Layouts --------------------------------------------

// Containers ---------------------------

// Styles --------------------------------------
import { globalStyle } from './globalStyle'
import { ResultsContainer } from './Application/Results/ResultsContainer'

const styles = () => ({
  '@global': globalStyle
})

export const AppComponent = () => (
  <ResultsContainer />
)
AppComponent.displayName = 'AppComponent'

export const App = withStyles(styles)(AppComponent)
App.displayName = 'App'
