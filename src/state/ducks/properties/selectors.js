import { keySelectors } from '../../../Utils/keySelectors'
import { keyNames } from '../../../constants/storeKeys'

const store = s => s[keyNames.properties]

export const {
  properties
} = keySelectors(store, ['properties'])
