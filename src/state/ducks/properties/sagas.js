import { all, put, takeLatest, call } from 'redux-saga/effects'

import { propertiesActions } from './reducers'
import { req } from '../../../Utils/axios'
import mock from '../../../Utils/mock.json'


class PropertyListing {
  constructor(
      { address, neighborhood, propertyType },
      { listPrice, daysListed, listingPhoto },
      { bedroomCount, bathroomCount },
      { houseStyle, numStories, masterOnMain, yearBuilt }, 
      { lotSize, squareFootage},
      { crimeCount, walkability, outdoorLiving }
    )
    
  {
    this.address = address
    this.neighborhood = neighborhood
    this.propertyType = propertyType

    this.listPrice = listPrice
    this.daysListed = daysListed
    this.listingPhoto = listingPhoto

    this.bedroomCount = bedroomCount
    this.bathroomCount = bathroomCount

    this.houseStyle = houseStyle
    this.numStories = numStories
    this.masterOnMain = masterOnMain
    this.yearBuilt = yearBuilt

    this.lotSize = lotSize
    this.squareFootage = squareFootage

    this.crimeCount = crimeCount
    this.walkability = walkability
    this.outdoorLiving = outdoorLiving

    // this.score = this.calculateScore()

    // calculateScore(test) {
    //   console.log('woohoo', test)
    // }
  }
}

class Preference {
  constructor(feature, score, acceptable) {
    this.feature = feature
    this.score = score
    this.acceptable = acceptable
  }
}

class Buyer {
  constructor(name, email, phone, preferences, commuteAddress) {
    this.name = name
    this.email = email
    this.phone = phone
    this.preferences = preferences
    this.commuteAddress = commuteAddress
  }
}

export function * resolveProperties () {
  try {
    const results = { data: mock }
    // const results = yield call(async () => req.get('/list-for-sale?is_matterports=false&beds_min=2&price_min=300000&sort=relevance&prop_type=single_family&baths_min=2&radius=1&price_max=715000&age_max=100&is_contingent=false&city=Nashville&limit=1000&offset=0&state_code=Tennessee'))
    console.log(results)
    
    const resultData = results.data.properties.map(prop => (new PropertyListing(
      { 
        address: `${prop.address.line} [${prop.address.postal_code}]`,
        neighborhood: prop.address.neighborhood_name
      },
      {
        listingPrice: 30000,
        daysListed: null,
        listingPhoto: ''
      },
      {
        bedroomCount: prop.beds,
        bathroomCount: prop.baths_full + ((prop.baths - prop.baths_full) / 2)
      },
      {
        houseStyle: null,
        numStories: null, 
        masterOnMain: null,
        yearBuilt: null
      },
      { 
        lotSize: prop.lot_size ? prop.lot_size.size : null,
        squareFootage: prop.building_size.size
      },
      {
        crimeCount: null,
        walkability: null,
        outdoorLiving: null
      }
    ))).map(prop => ({ ...prop, image: prop.rdc_web_url}))

    yield put(propertiesActions.getPropertiesSuccess(resultData))
  } catch (e) {
    console.log(e)
    yield put(propertiesActions.getPropertiesFailure())
  }
}

const getZillowData = () => {
  
}

export function * propertiesSaga () {
  yield all([
    takeLatest('GET_PROPERTIES', resolveProperties)
  ])
}
