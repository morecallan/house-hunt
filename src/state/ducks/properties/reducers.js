import produce from 'immer'
import { handleActions, createActions } from 'redux-actions'

export const defaultState = {
  properties: [],
  propertiesLoading: false
}

export const propertiesActions = createActions(
  {
    GET_PROPERTIES_SUCCESS: (values) => (values),
  },
  'GET_PROPERTIES',
  'GET_PROPERTIES_FAILURE'
)

export const reducer = handleActions(
  {
    GET_PROPERTIES: (state) => produce(state, draft => {
      draft.propertiesLoading = true
    }),
    GET_PROPERTIES_SUCCESS: (state, { payload }) => produce(state, draft => {
      draft.properties = payload
      draft.propertiesLoading = false
    }),
    GET_PROPERTIES_FAILURE: (state) => produce(state, draft => {
      draft.propertiesLoading = false
    })
  },
  defaultState
)
