import { all, fork } from 'redux-saga/effects'

import { propertiesSaga } from './ducks/properties/sagas'

export function * indexSaga () {
  yield all([
    fork(propertiesSaga)
  ])
}
