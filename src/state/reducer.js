import { keyNames } from '../constants/storeKeys'

import { reducer as propertiesReducer } from './ducks/properties'


export const rootReducer = {
  [keyNames.properties]: propertiesReducer
}
