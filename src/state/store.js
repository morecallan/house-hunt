import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'

import { indexSaga } from './sagas'
import { rootReducer } from './reducer'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const reducer = combineReducers({ ...rootReducer })

const sagaMiddleware = createSagaMiddleware()

const getAppliedMiddlewares = () => {
  const middleware = [sagaMiddleware]

  if (process.env.NODE_ENV === 'development') {
    const { createLogger } = require('redux-logger')
    middleware.push(createLogger({ collapsed: true }))
  }

  return applyMiddleware(...middleware)
}

export const configureStore = (initialState = {}, rootSagaArg) => {
  const composedMiddleware = composeEnhancer(getAppliedMiddlewares())
  const store = createStore(reducer, initialState, composedMiddleware)

  sagaMiddleware.run(rootSagaArg || indexSaga)

  if (module.hot) {
    module.hot.accept('./reducer', () => {
      store.replaceReducer(require('./reducer').default)
    })
  }
  return store
}

export const store = configureStore()
